﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Util
{
    public static class Vector2RectExtensions
    {
        public static bool IsInsideRect(this Vector2 v, Vector2 location, Vector2 size)
        {
            return
                v.X >= location.X && v.X <= location.X + size.X &&
                v.Y >= location.Y && v.Y <= location.Y + size.Y;
        }

        public static bool IsInsideRect(this Vector2 v, IntVector2 location, IntVector2 size)
        {
            return
                v.X >= location.X && v.X <= location.X + size.X &&
                v.Y >= location.Y && v.Y <= location.Y + size.Y;
        }

        public static Rect ToRectWithEndPoint(this Vector2 v, IntVector2 endPoint)
        {
            var size = endPoint - v;
            return new Rect(v.X, v.Y, size.X, size.Y);
        }

        public static Rect ToRectWithEndPoint(this Vector2 v, Vector2 endPoint)
        {
            var size = endPoint - v;
            return new Rect(v.X, v.Y, size.X, size.Y);
        }

        public static Rect ToRectWithSize(this Vector2 v, IntVector2 size)
        {
            return new Rect(v.X, v.Y, size.X, size.Y);
        }

        public static Rect ToRectWithSize(this Vector2 v, Vector2 size)
        {
            return new Rect(v.X, v.Y, size.X, size.Y);
        }
    }
}
