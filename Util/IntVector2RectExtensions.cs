﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Util
{
    public static class IntVector2RectExtensions
    {
        public static Rect ToRectWithEndPoint(this IntVector2 v, IntVector2 endPoint)
        {
            var size = endPoint - v;
            return new Rect(v.X, v.Y, size.X, size.Y);
        }

        public static Rect ToRectWithEndPoint(this IntVector2 v, Vector2 endPoint)
        {
            var size = endPoint - v;
            return new Rect(v.X, v.Y, size.X, size.Y);
        }

        public static Rect ToRectWithSize(this IntVector2 v, IntVector2 size)
        {
            return new Rect(v.X, v.Y, size.X, size.Y);
        }

        public static Rect ToRectWithSize(this IntVector2 v, Vector2 size)
        {
            return new Rect(v.X, v.Y, size.X, size.Y);
        }
    }
}
