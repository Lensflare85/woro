﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util.Random
{
    public interface IRandomNumberGenerator
    {
        /// <summary>
        /// Returns a non-negative random integer.
        /// </summary>
        int Next();

        /// <summary>
        /// Returns a non-negative random integer that is less than the specified maximum.
        /// </summary>
        int Next(int maxValue);

        /// <summary>
        /// Returns true if an event has occured with the specified probability.
        /// </summary>
        bool PercentChance(int percentage);
    }
}
