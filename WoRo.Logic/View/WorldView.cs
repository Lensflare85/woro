﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoRo.Logic.Screen;

namespace WoRo.Logic.View
{
    public class WorldView
    {
        SmoothViewPort worldViewPort = new SmoothViewPort();
        public SmoothViewPort ViewPort => worldViewPort;

        /*public float Zoom
        {
            get
            {
                return worldViewPort.SpaceZoom;
            }
            set
            {
                worldViewPort.SpaceZoom = value;
            }
        }*/
    }
}
