﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.Foundation;

namespace WoRo.Logic.Screen
{
    public class ViewPort
    {
        public IntVector2 Location;
        public IntVector2 Size;

        public Rect Rect => Location.ToRectWithSize(Size);

        protected float intermediateSpaceZoom = 1;

        public float SpaceZoom {
            get
            {
                return (float)Math.Round(intermediateSpaceZoom, 3);
            }
            set => intermediateSpaceZoom = value;
        }

        protected Vector2 intermediateSpaceOffset;
        public Vector2 SpaceOffset
        {
            get => intermediateSpaceOffset;
            set => intermediateSpaceOffset = value;
        }
    }
}
