﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoRo.Logic
{
    public enum InputAction
    {
        Left, Right, Up, Down,
        Rope,
    }
}
