﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoRo.Logic.Entities;
using WoRo.Logic.View;

namespace WoRo.Logic
{
    public class Game
    {
        public readonly WorldView WorldView = new WorldView();

        private List<Entity> entities = new List<Entity>();
        public IEnumerable<Entity> Entities => entities;

        public void Add(Entity entity)
        {
            entities.Add(entity);
        }

        public void Remove(Entity entity)
        {
            entities.Remove(entity);
        }
    }
}
