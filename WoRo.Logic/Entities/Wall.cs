﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace WoRo.Logic.Entities
{
    public class Wall : Entity
    {
        public Vector2 Start;
        public Vector2 End;
    }
}
