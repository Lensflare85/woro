﻿using XnaVector2 = Microsoft.Xna.Framework.Vector2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Collision;
using FarseerPhysics.Common;
using FarseerPhysics.Controllers;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework.Input;
using Windows.UI.Core;
using WoRo.Logic.Entities;
using WoRo.Logic;
using System.Numerics;
using Util;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Collision.Shapes;

// Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x407 dokumentiert.

namespace WoRo
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Game game;

        private World world;
        private Body agentBody;

        private Body ropeProjectileBody;

        private Body ropeAnchorBody;
        private Body ropeSegmentBody;
        private Joint ropeJoint1;
        private Joint ropeJoint2;
        private DistanceJoint ropeDistanceJoint;
        private RopeSegment ropeSegment;

        const float ropeThickness = 0.02f;
        const float ropeDensity = 0.01f;

        private Agent agent;

        private bool pressingLeft;
        private bool pressingRight;
        private bool pressingUp;
        private bool pressingDown;

        private bool ropeKeyChangedToDown = false;

        private Vector2 cameraVelocity = new Vector2();

        private InputKeyMap inputKeyMap = new InputKeyMap();

        public MainPage()
        {
            this.InitializeComponent();

            game = new Game();
            game.WorldView.ViewPort.SpaceZoom = 100;
            game.WorldView.ViewPort.Location = new IntVector2(0, 0);
            game.WorldView.ViewPort.Size = new IntVector2(canvas.Size.ToVector2());
        }

        private void canvas_CreateResources(Microsoft.Graphics.Canvas.UI.Xaml.CanvasAnimatedControl sender, Microsoft.Graphics.Canvas.UI.CanvasCreateResourcesEventArgs args)
        {
            
        }

        private async void canvas_Draw(Microsoft.Graphics.Canvas.UI.Xaml.ICanvasAnimatedControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasAnimatedDrawEventArgs args)
        {
            var zoom = game.WorldView.ViewPort.SpaceZoom;
            var spaceOffset = game.WorldView.ViewPort.SpaceOffset;
            var screenOffset = game.WorldView.ViewPort.Size * 0.5f;

            foreach (var entity in game.Entities.ToArray())
            {
                switch (entity)
                {
                    case Wall wall:
                        {
                            args.DrawingSession.DrawLine((wall.Start - spaceOffset) * zoom + screenOffset, (wall.End - spaceOffset) * zoom + screenOffset, Colors.Blue);
                        }
                        break;
                    case RopeSegment ropeSegment:
                        {
                            var body = (Body)ropeSegment.PhysicsData;
                            var start = ropeSegment.Start;
                            var end = ropeSegment.End;
                            var length = (end - start).Length();
                            var halfLength = length * 0.5f;
                            var angle = body.Rotation;
                            var center = body.Position;
                            var halfDelta = new XnaVector2((float)Math.Cos(angle) * halfLength, (float)Math.Sin(angle) * halfLength);
                            var p1 = center - halfDelta;
                            var p2 = center + halfDelta;
                            args.DrawingSession.DrawLine((p1.System() - spaceOffset) * zoom + screenOffset, (p2.System() - spaceOffset) * zoom + screenOffset, Colors.Orange);
                        }
                        break;
                    case Agent agent:
                        {
                            var body = (Body)agent.PhysicsData;
                            var position = (body.Position.System() - spaceOffset) * zoom + screenOffset;
                            args.DrawingSession.DrawCircle(position, agent.Size * zoom, Colors.Aqua);
                        }
                        break;
                    case RopeProjectile ropeProjectile:
                        {
                            var body = (Body)ropeProjectile.PhysicsData;
                            var position = (body.Position.System() - spaceOffset) * zoom + screenOffset;
                            args.DrawingSession.DrawCircle(position, ropeProjectile.Size * zoom, Colors.Red);
                        }
                        break;
                    case CircleObstacle obstacle:
                        {
                            var body = (Body)obstacle.PhysicsData;
                            var position = (body.Position.System() - spaceOffset) * zoom + screenOffset;
                            args.DrawingSession.DrawCircle(position, obstacle.Size * zoom, Colors.Green);
                        }
                        break;
                }
            }

            /*
            foreach(var joint in world.JointList.ToArray())
            {
                args.DrawingSession.DrawLine((joint.WorldAnchorA.System() - spaceOffset) * zoom + screenOffset, (joint.WorldAnchorB.System() - spaceOffset) * zoom + screenOffset, Colors.Yellow);
            }*/

            /*await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, delegate
            {
                
            });*/
        }

        private async void canvas_Update(Microsoft.Graphics.Canvas.UI.Xaml.ICanvasAnimatedControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasAnimatedUpdateEventArgs args)
        {
            var elapsedTime = args.Timing.ElapsedTime;

            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, delegate {
                var agentViewDelta = agentBody.Position.System() - game.WorldView.ViewPort.SpaceOffset;
                cameraVelocity = agentViewDelta * 0.1f;
                game.WorldView.ViewPort.SpaceOffset += cameraVelocity;

                if (ropeSegmentBody != null && ropeSegment != null)
                {
                    var center = ropeSegmentBody.Position;
                    var angle = ropeSegmentBody.Rotation;
                    var delta = ropeSegment.End - ropeSegment.Start;
                    var halfLength = delta.Length() * 0.5f;
                    var halfDelta = new XnaVector2((float)Math.Cos(angle) * halfLength, (float)Math.Sin(angle) * halfLength);
                    var p1 = center - halfDelta;
                    if ((agentBody.Position - p1).Length() > agent.Size)
                    {
                        agentBody.Position = p1;
                    }

                    var forceStrength = 0.7f;
                    var perpendicularForce = new XnaVector2((float)-Math.Sin(angle) * forceStrength, (float)Math.Cos(angle) * forceStrength);

                    var verticalStrength = 0.07f;
                    var verticalForceStrength = 100f;

                    if (pressingRight)
                    {
                        agentBody.ApplyForce(perpendicularForce);
                    }
                    if (pressingLeft)
                    {
                        agentBody.ApplyForce(-perpendicularForce);
                    }
                    if (pressingUp)
                    {
                        var normal = halfDelta;
                        normal.Normalize();
                        var change = (normal.System() * verticalStrength).Xna();
                        var newP1 = (p1.System() + change.System()).Xna();
                        var fixture = ropeSegmentBody.FixtureList.First();
                        /*var shape = fixture.Shape as PolygonShape;
                        var vertices = shape.Vertices;
                        var v0 = vertices.ElementAt(0);
                        var v1 = vertices.ElementAt(1);
                        var v2 = vertices.ElementAt(2);
                        var v3 = vertices.ElementAt(3);
                        vertices[0] = v0 + new XnaVector2(-change.Length() * 0.5f, 0);
                        vertices[1] = v1 + new XnaVector2(-change.Length() * 0.5f, 0);
                        vertices[2] = v2 + new XnaVector2(change.Length() * 0.5f, 0);
                        vertices[3] = v3 + new XnaVector2(change.Length() * 0.5f, 0);*/

                        //ropeSegmentBody.DestroyFixture(fixture);
                        //FixtureFactory.AttachRectangle((ropeSegment.End - ropeSegment.Start).Length() - verticalStrength, ropeThickness, ropeDensity, ropeSegmentBody.Position, ropeSegmentBody);

                        world.RemoveJoint(ropeDistanceJoint);

                        agentBody.Position += change;

                        //agentBody.ApplyForce(change * verticalForceStrength);
                        //agentBody.ApplyLinearImpulse(change * verticalForceStrength);

                        AddRope(ropeAnchorBody);

                        //ropeDistanceJoint.WorldAnchorA = newP1;

                        /*
                        ropeJoint1.WorldAnchorA = agentBody.Position;
                        ropeJoint1.WorldAnchorB = agentBody.Position;
                        ropeJoint2.WorldAnchorA = ropeSegment.End.Xna();
                        ropeJoint2.WorldAnchorB = ropeSegment.End.Xna();
                        */

                        //ropeSegmentBody.Position += change;
                        //ropeSegment.Start = agentBody.Position.System(); //newP1.System();

                        //var delta = halfDelta.System() * 2;
                    }
                    if (pressingDown)
                    {
                        var normal = halfDelta;
                        normal.Normalize();
                        var change = (normal.System() * -verticalStrength).Xna();
                        world.RemoveJoint(ropeDistanceJoint);

                        agentBody.Position += change;
                        //agentBody.ApplyForce(change * verticalForceStrength);
                        //agentBody.ApplyLinearImpulse(change * verticalForceStrength);

                        AddRope(ropeAnchorBody);
                    }
                }
                else
                {
                    /*
                    var force = 4.0f;

                    if (pressingRight)
                    {
                        agentBody.ApplyForce(new Vector2(force, 0).Xna());
                    }
                    if (pressingLeft)
                    {
                        agentBody.ApplyForce(new Vector2(-force, 0).Xna());
                    }
                    if (pressingUp)
                    {
                        agentBody.ApplyForce(new Vector2(0, -force).Xna());
                    }
                    if (pressingDown)
                    {
                        agentBody.ApplyForce(new Vector2(0, force).Xna());
                    }*/
                }

                world.Step((float)elapsedTime.TotalSeconds);
            
                //fpsTextBlock.Text = "FPS: " + fps;
            });
        }

        private void canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            game.WorldView.ViewPort.Size = new IntVector2(canvas.Size.ToVector2());
        }

        private void AddWall(Vector2 start, Vector2 end)
        {
            var wall = new Wall() { Start = start, End = end };
            var edge = BodyFactory.CreateEdge(world, start.Xna(), end.Xna(), wall);
            wall.PhysicsData = edge;
            game.Add(wall);
        }

        private InputAction? ActionForKey(Windows.System.VirtualKey key)
        {
            foreach(var pair in inputKeyMap.Map)
            {
                if (pair.Value.Contains(key))
                {
                    return pair.Key;
                }
            }

            return null;
        }

        private void ShootRope()
        {
            var ropeProjectile = new RopeProjectile() { Size = 0.04f };
            ropeProjectileBody = BodyFactory.CreateCircle(world, ropeProjectile.Size, 1, agentBody.Position, BodyType.Dynamic, ropeProjectile);
            ropeProjectileBody.LinearVelocity = agentBody.LinearVelocity + new XnaVector2(0,-40);
            ropeProjectileBody.OnCollision += (fixture1, fixture2, contact) =>
            {
                XnaVector2 normal;
                FixedArray2<XnaVector2> points;
                contact.GetWorldManifold(out normal, out points);
                if (contact.Manifold.PointCount > 0)
                {
                    var contactPoint = points[0];
                    /*
                    var correctionOffset = body.LinearVelocity;
                    correctionOffset.Normalize();
                    contactPoint += correctionOffset * 0.02f;
                    */

                    ropeAnchorBody = BodyFactory.CreateCircle(world, ropeProjectile.Size, 0, contactPoint, BodyType.Static, null);
                    ropeAnchorBody.CollisionCategories = Category.None;
                    
                    game.Remove(ropeProjectile);
                    world.RemoveBody(ropeProjectileBody);
                    ropeProjectileBody = null;

                    var ropeAdded = AddRope(ropeAnchorBody);

                    if (!ropeAdded)
                    {
                        world.RemoveBody(ropeAnchorBody);
                        ropeAnchorBody = null;
                    }
                }
                return true;
            };
            ropeProjectileBody.IgnoreCollisionWith(agentBody);
            ropeProjectile.PhysicsData = ropeProjectileBody;
            game.Add(ropeProjectile);
        }

        private bool AddRope(Body anchor)
        {
            if (ropeSegmentBody != null)
            {
                world.RemoveBody(ropeSegmentBody);
                world.RemoveJoint(ropeJoint1);
                world.RemoveJoint(ropeJoint2);
                game.Remove(ropeSegment);
            }

            var start = agentBody.Position;
            var end = anchor.Position;
            ropeSegment = new RopeSegment() { Start = start.System(), End = end.System() };

            var d = end - start;
            var position = start + d * 0.5f;
            var rotation = (float)Math.Atan2(d.Y, d.X);
            var width = d.Length();
            if (width > 0)
            {
                ropeSegmentBody = BodyFactory.CreateRectangle(world, width, ropeThickness, ropeDensity, position, rotation, BodyType.Dynamic, ropeSegment);
                ropeSegmentBody.IgnoreCollisionWith(agentBody);
                ropeSegment.PhysicsData = ropeSegmentBody;
                game.Add(ropeSegment);

                ropeJoint1 = JointFactory.CreateRevoluteJoint(world, agentBody, ropeSegmentBody, start, start, true);
                ropeJoint2 = JointFactory.CreateRevoluteJoint(world, anchor, ropeSegmentBody, end, end, true);

                ropeDistanceJoint = JointFactory.CreateDistanceJoint(world, agentBody, anchor);

                return true;
            }

            return false;
        }

        private void AddCircleObstacle()
        {
            var obstacle = new CircleObstacle() { Size = 0.3f };
            var body = BodyFactory.CreateCircle(world, obstacle.Size, 1, agentBody.Position, BodyType.Static, obstacle);
            body.Position = new XnaVector2(8, 4);
            obstacle.PhysicsData = body;
            game.Add(obstacle);
        }

        private void canvas_Loaded(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Loaded ");

            world = new World(new Vector2(0, 10).Xna());

            agent = new Agent() { Size = 0.2f };

            agentBody = BodyFactory.CreateCircle(world, agent.Size, 1, new Vector2(1, 2).Xna(), BodyType.Dynamic, agent);
            agentBody.FixedRotation = true;

            agent.PhysicsData = agentBody;

            game.Add(agent);

            var bottomY = 8.0f;
            var rightX = 16.0f;

            AddWall(new Vector2(0, 0), new Vector2(rightX, 0));
            AddWall(new Vector2(0, bottomY), new Vector2(rightX, bottomY));
            AddWall(new Vector2(0, 0), new Vector2(0, bottomY));
            AddWall(new Vector2(rightX, 0), new Vector2(rightX, bottomY));

            AddCircleObstacle();

            CoreWindow.GetForCurrentThread().KeyDown += (cw, args) => {
                if (args.KeyStatus.WasKeyDown)
                {
                    return;
                }
                switch (ActionForKey(args.VirtualKey))
                {
                    case InputAction.Right:
                        pressingRight = true;
                        break;
                    case InputAction.Left:
                        pressingLeft = true;
                        break;
                    case InputAction.Up:
                        pressingUp = true;
                        break;
                    case InputAction.Down:
                        pressingDown = true;
                        break;
                    case InputAction.Rope:
                        ropeKeyChangedToDown = true;
                        Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, delegate
                        {
                            if (ropeAnchorBody != null)
                            {
                                game.Remove(ropeSegment);
                                world.RemoveJoint(ropeJoint1);
                                world.RemoveJoint(ropeJoint2);
                                world.RemoveBody(ropeSegmentBody);
                                world.RemoveBody(ropeAnchorBody);

                                ropeSegment = null;
                                ropeJoint1 = null;
                                ropeJoint2 = null;
                                ropeSegmentBody = null;
                                ropeAnchorBody = null;
                            }
                            else
                            {
                                if (ropeProjectileBody == null)
                                {
                                    ShootRope();
                                }
                            }
                        });
                        break;
                }
            };

            CoreWindow.GetForCurrentThread().KeyUp += (cw, args) => {
                switch (ActionForKey(args.VirtualKey))
                {
                    case InputAction.Right:
                        pressingRight = false;
                        break;
                    case InputAction.Left:
                        pressingLeft = false;
                        break;
                    case InputAction.Up:
                        pressingUp = false;
                        break;
                    case InputAction.Down:
                        pressingDown = false;
                        break;
                }
            };
        }

        private void canvas_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            switch (e.Key)
            {
                case Windows.System.VirtualKey.Right:
                    pressingRight = true;
                    break;
            }
        }

        private void canvas_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            switch (e.Key)
            {
                case Windows.System.VirtualKey.Right:
                    pressingRight = false;
                    break;
            }
        }

        private void Page_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            switch (e.Key)
            {
                case Windows.System.VirtualKey.Right:
                    pressingRight = true;
                    break;
            }
        }

        private void Page_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            switch (e.Key)
            {
                case Windows.System.VirtualKey.Right:
                    pressingRight = false;
                    break;
            }
        }

        private void Page_PreviewKeyDown(object sender, KeyRoutedEventArgs e)
        {
            int i = 0;
        }
    }
}
