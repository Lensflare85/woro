﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoRo
{
    public static class Vector2Conversion
    {
        public static Microsoft.Xna.Framework.Vector2 Xna(this System.Numerics.Vector2 v)
        {
            return new Microsoft.Xna.Framework.Vector2(v.X, v.Y);
        }

        public static System.Numerics.Vector2 System(this Microsoft.Xna.Framework.Vector2 v)
        {
            return new System.Numerics.Vector2(v.X, v.Y);
        }
    }
}
