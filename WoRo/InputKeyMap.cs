﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoRo.Logic;
using Windows.System;

namespace WoRo
{
    public class InputKeyMap
    {
        private Dictionary<InputAction, IEnumerable<VirtualKey>> map = new Dictionary<InputAction, IEnumerable<VirtualKey>>
        {
            { InputAction.Left, new [] { VirtualKey.Left, VirtualKey.A, VirtualKey.NumberPad4 } },
            { InputAction.Right, new [] { VirtualKey.Right, VirtualKey.D, VirtualKey.NumberPad6 } },
            { InputAction.Up, new [] { VirtualKey.Up, VirtualKey.W , VirtualKey.NumberPad8 } },
            { InputAction.Down, new [] { VirtualKey.Down, VirtualKey.S, VirtualKey.NumberPad5, VirtualKey.NumberPad2 } },
            { InputAction.Rope, new [] { VirtualKey.Space } }
        };

        public IReadOnlyDictionary<InputAction, IEnumerable<VirtualKey>> Map => map;
    }
}
